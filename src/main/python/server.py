import http.server

if __name__ == "__main__":
    # start the server on port 8000
    print("Example webserver running")
    daemon = http.server.HTTPServer(('', 8000), http.server.SimpleHTTPRequestHandler)
    daemon.serve_forever()
