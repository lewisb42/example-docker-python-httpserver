from pybuilder.core import use_plugin
from pybuilder.core import init
from pybuilder.core import Author
from pybuilder.core import task, depends

from subprocess import call
from os import path

# plugins for common tasks
use_plugin('python.core')
use_plugin('python.unittest')
use_plugin('python.install_dependencies')
use_plugin('python.distutils')
use_plugin('copy_resources')

# the task run when pyb is executed
default_task = 'publish'

# target directory (relative)
target_dir = path.join('target', 'dist', 'app')

@init
def normalize_target_dir(project):
    project.set_property('dir_dist', target_dir)

@init
def copy_html_pages_to_distribution(project):
    project.set_property('copy_resources_target', target_dir)
    project.set_property('copy_resources_glob', ['*.html', '*.HTML', '*.htm', '*.HTM'])

@init
def initialize(project):
    # project information
    project.name = 'Docker Example'
    project.version = '1.0'
    project.summary = 'In-class exercise for CS3280'
    project.description = '''
        Simple python http.server running from a Docker container.
    '''
    project.authors = [Author('Lewis Baumstark', 'lewisb@westga.edu')]
    project.license = "not for redistribution"
    project.url = 'https://www.westga.edu/academics/cosm/computer-science/'
    
@depends('publish')
@task
def launch_server(project):
    print("Note: This webserver is not running in a container.")
    print("Note: A webpage should be available at http://127.0.0.1:8000/facts.html")
    call([
        'python',
        path.join(target_dir, 'server.py')
    ])
    
# command to build the image (put here for example purposes). Assumes Dockerfile in getcwd()
'''
docker build --tag example_image .
'''

# command to run the container (put here for example purposes). Assumes image built above.
'''
docker run -d -p 8000:8000 -v /home/student/example-docker-python-httpserver/target/dist/app/:/app --name example_container example_image
'''
